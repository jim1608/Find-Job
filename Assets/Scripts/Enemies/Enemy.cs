﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;
using System;
using System.Linq;
public class Enemy : MonoBehaviour {

    public Statics.TYPE_ENEMY MyType;
    public Life _life;
    public GameObject Explosion;
    public int Damage = 1;
    public Shot shot;
    public GameObject ItemDrop;
    public bool canDodge = false;
    public float dodgeRange = 400f;

    bool dodgeDirectionPositive = false;


    // Use this for initialization
    void Start() {

        // Definimos aleatóriamente no começo a direçao que cada nave desvia. Assim eles ficam um pouco menos previsíveis
        dodgeDirectionPositive = UnityEngine.Random.value > .5f ? true : false;

        if (MyType == Statics.TYPE_ENEMY.SHIP)
        {
            StartCoroutine(Shoot());
        }
    }

    private void Update()
    {
        if (!Statics.Player) return;
        if (MyType == Statics.TYPE_ENEMY.SHIP)
        {
            Quaternion q = Statics.FaceObject(Statics.Player.localPosition, transform.localPosition, Statics.FacingDirection.Right);
            transform.rotation = q;
        }
    }

    //Vamos usar Fixed update para checar se algum tiro está chegando e se precisamos desviar.
    //Usamos Fixed update para consumir menos recursos
    private void FixedUpdate()
    {
        //Se o inimigo pode desviar do tiro, assim que o tiro chegar perto dele o inimigo vai se movimentar na direção perpendicular ao tiro.
        if (canDodge)
        {
            //Pega todos os tiros próximos o suficiente desse inimigo.
            List<Collider2D> colliders = Physics2D.OverlapCircleAll(transform.position, dodgeRange).ToList().FindAll(x => x.gameObject.tag == "Shot");

            if (colliders.Count > 0)
            {
                //Devido a natureza do jogo, raramente (talvez nunca) os tiros do player vão se aproximar de angulos diferentes do inimigo.
                //Além disso na maioria dos casos este check vai apeans pegar 1 ou 2 tiros a cada Fixed Update.
                //sendo assim qualquer tiro da lista nos dá uma boa estimativa da direçao que precisamos desviar.
                //Arbitrariamente pegaremos logo o primeiro tiro da lista.
                //(Nota: Alternativamente poderiamos pegar ordená-los para pegar o mais próximo ou fazer a média entre essas posiçoes)

                //Pegamos então o vetor normalizado perpendicular a subtraçao da distancia entre o tiro e o inimigo. Assim temos a direçao tangencial ao tiro.
                Vector2 MoveDirPerp = Vector2.Perpendicular((colliders[0].gameObject.transform.localPosition - transform.localPosition));

                //Para deixar mais interessante no Start definimos se esse inimigo prioriza desviar para direita ou esquerda
                //Assim fica pouco previsivel MAS também evita que o inimigo fique pulando de volta na direçao contrária após desviar de um tiro
                if (!dodgeDirectionPositive) MoveDirPerp *= -1;

                //agora movemos para longe do objeto. Podemos diminuir a velocidade se o Designer achar interessante
                GetComponent<Rigidbody2D>().MovePosition((Vector2)transform.position + MoveDirPerp * (700f));

            }
        }
    }

    void OnDrawGizmosSelected()
	{
        Gizmos.color = Color.red;
        if(canDodge) Gizmos.DrawWireSphere(this.transform.position, dodgeRange);
	}

    private void OnCollisionEnter2D(Collision2D objeto)
    {
        if (objeto.gameObject.tag == "Shot")
        {
            Destroy(objeto.gameObject);
            _life.TakesLife(Statics.Damage);
            Instantiate(Explosion, transform).transform.parent = transform.parent;
        }
    }


    public void MyDeath()
    {
        //Explosao;
        if(MyType == Statics.TYPE_ENEMY.METEOR)
        {
            if (UnityEngine.Random.Range(0, 100) > 60)//60% chance of it becoming bits and pieces
            {
                Create(1);
                Create(2);
                Create(3);
                Create(4);
            }
            Statics.EnemiesDead++;
            
        }
        if(ItemDrop != null && UnityEngine.Random.Range(0, 100) > 70)
        {
            Instantiate(ItemDrop, transform.parent).transform.localPosition = transform.localPosition;
        }
        Statics.Points += Damage * 100;
        StartCoroutine(KillMe());
    }

    private IEnumerator KillMe()
    {
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }

    private void Create(int v)
    {
        
        GameObject goMunus = Instantiate(gameObject, transform.parent);
        goMunus.GetComponent<Enemy>().MyType = Statics.TYPE_ENEMY.PIECES;
        float scale = UnityEngine.Random.Range(0.2f, 0.6f);
        goMunus.transform.localScale = new Vector3(scale, scale, scale);
        float force = 100f * goMunus.GetComponent<Rigidbody2D>().mass;
        switch (v) {
           case 1: goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.down * force, ForceMode2D.Impulse);
                break;
            case 2:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * force, ForceMode2D.Impulse);
                break;
            case 3:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * force, ForceMode2D.Impulse);
                break;
            case 4:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * force, ForceMode2D.Impulse);
                break;
        }
    }


    private IEnumerator Shoot()
    {
        //Pode executar até 3 tiros simultaneos
        // Increased speed here and in inspector. Not clear if wanted one, the other or both in the instructions.
        // For boss2 increased only where was possible (script)
        //Also fixed the tag for enemy shots
        while (true)
        {
            yield return new WaitForSeconds(shot.ShootingPeriod);
            
                Statics.Damage = shot.Damage;
                GameObject goShooter = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                goShooter.transform.parent = transform;
                goShooter.transform.localPosition = shot.Weapon.transform.localPosition;
                goShooter.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 16000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShooter.AddComponent<BoxCollider2D>();
                goShooter.transform.parent = transform.parent;
                goShooter.tag = "EnemyShot";

                if (shot.TypeShooter == Statics.TYPE_SHOT.DOUBLE)
                {
                    GameObject goShooter2 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter2.transform.parent = transform;
                    goShooter2.transform.localPosition = shot.Weapon2.transform.localPosition;
                    goShooter2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 16000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter2.AddComponent<BoxCollider2D>();
                    goShooter2.transform.parent = transform.parent;
                    goShooter2.tag = "EnemyShot";
            }

                if (shot.TypeShooter == Statics.TYPE_SHOT.TRIPLE)
                {
                    GameObject goShooter2 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter2.transform.parent = transform;
                    goShooter2.transform.localPosition = shot.Weapon2.transform.localPosition;
                    goShooter2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 16000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter2.AddComponent<BoxCollider2D>();
                    goShooter2.transform.parent = transform.parent;
                    goShooter2.tag = "EnemyShot";

                    GameObject goShooter3 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter3.transform.parent = transform;
                    goShooter3.transform.localPosition = shot.Weapon3.transform.localPosition;
                    goShooter3.GetComponent<Rigidbody2D>().AddForce(transform.up * ((shot.SpeedShooter * 16000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter3.AddComponent<BoxCollider2D>();
                    goShooter3.transform.parent = transform.parent;
                    goShooter3.tag = "EnemyShot";
            }
            

            
        }
    }

    public void EndBoss()
    {
        GameObject.Find("Control").GetComponent<ControlGame>().LevelPassed();
        Destroy(gameObject);
        Invoke("PStick", 1f);
    }

    /*
    void PStick()
    {

        if(Stick.GetStck() == Stick.stck.MANESTIC)
        {
            Update();
        }
    }*/
}

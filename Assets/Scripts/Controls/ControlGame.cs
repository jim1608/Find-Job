﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;
    public GameObject PauseUI;
    private bool isPaused = false;
    private bool canPause;

    // Use this for initialization
    void Start() {
        canPause = true;
        Statics.EnemiesDead = 0;
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);

    }

    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);

        if (Input.GetKeyDown(KeyCode.Return) && canPause)
        {
            if (isPaused)
            {
                PauseUI.SetActive(false);
                Time.timeScale = 1;
                GetComponent<AudioSource>().UnPause();
            }
            else
            {
                PauseUI.SetActive(true);
                Time.timeScale = 0;
                GetComponent<AudioSource>().Pause();
            }
            isPaused = !isPaused;
        }
    }

    public void LevelPassed()
    {
        canPause = false;
        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;

        //O jogo salva o score apenas depois de passar de um level
        //Assim evitamos que um player fique no level 1 inflando seu score.
        GameSave.data.latestScore = Statics.Points;
        GameSave.data.savedLevel = Statics.CurrentLevel;

        //Salvamos o jogo aqui
        //Do jeito que está programado, ele só salva High Score quando o player vence o jogo todo
        //Não vou mudar isso, mas no Record control vou adicionar lógica para salvar o HighScore junto do save.
        GameSave.SaveGameData();

        if (Statics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        canPause = false;
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

}

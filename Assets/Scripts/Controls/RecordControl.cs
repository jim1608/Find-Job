﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordControl : MonoBehaviour {
    public Text TxtRecord;
    public InputField InputRecord;
	// Use this for initialization
	void Start () {
        //editamos isso aqui para usar o high score no save file
        //
        InputRecord.text = GameSave.data.playerName;
        InputRecord.interactable = false;
        if (Statics.Points > GameSave.data.highestScore)
        {


            GameSave.data.highestScore = Statics.Points;
            InputRecord.interactable = true;
            InputRecord.text = "";
        }

        TxtRecord.text = "Your Points: " + Statics.Points+"\nRecord: " + GameSave.data.highestScore.ToString();

    }
	
	public void UpdateName()
    {
        GameSave.data.playerName = InputRecord.text;
        GameSave.SaveGameData();

        PlayerPrefs.SetString(Statics.PLAYERPREF_NEWRECORD, InputRecord.text);
    }
}

﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlStart : MonoBehaviour {
    public Text Record;
    public Text ContinueText;
    public Button ContinueButton;
    // Use this for initialization
    void Start() {

        //Reset the variables to start the game from scratch
        Statics.WithShield = false;
        Statics.EnemiesDead = 0;
        Statics.CurrentLevel = 0;
        Statics.Points = 0;
        Statics.ShootingSelected = 2;

        //Checa se jogo tem save data
        GameSave.LoadGameData();
        //Carrega highest score
        //por padrão o jogo tem valores 0 como record salvo
        Record.text = "Record: " + GameSave.data.playerName + "(" + GameSave.data.highestScore + ")";

        //Se o player anterior passou do 1o level pelo menos ele pode continuar
        if (GameSave.data.savedLevel > 0 && GameSave.data.savedLevel < 3)
        {
            ContinueText.text = "Level: " + (GameSave.data.savedLevel + 1) + " (" + GameSave.data.latestScore + ")";
        }
		else {
            ContinueButton.gameObject.SetActive(false);
            ContinueText.gameObject.SetActive(false);
            //Se ele zerou o jogo, remove a possibilidade de Continue
            if(GameSave.data.savedLevel >= 3) {
                GameSave.data.latestScore = 0;
                GameSave.data.savedLevel = 0;
            }
        }



    }

    public void ContinueClick()
	{
        Statics.CurrentLevel = GameSave.data.savedLevel;
        Statics.Points = GameSave.data.latestScore;

        StartClick();
    }

	public void StartClick()
    {
        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    public void Quit()
    {
        Application.Quit();
    }

    
}

using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Gaminho
{
    [System.Serializable]
    public class SaveData
	{
        public int highestScore = 0;
        public int latestScore = 0;
        public int savedLevel = 0;
        public string playerName = "NoName";
	}

    public static class GameSave
    {
        public static bool loaded = false;
        public static SaveData data = new SaveData();

        public static bool LoadGameData()
        {
            loaded = true;
            data = new SaveData();
            if (File.Exists(Application.persistentDataPath + "/SavedGame.dat"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(Application.persistentDataPath + "/SavedGame.dat", FileMode.Open);
                SaveData newData = (SaveData)bf.Deserialize(file);
                file.Close();

                data = newData;

                Debug.Log("Loaded The File");

                return true;
            }
            else
            {
                Debug.Log("File Does Not Exist");
                return false;
            }
            
        }
        public static void SaveGameData()
        {
            Debug.LogWarning("Saving data as single Method. Please add a saving data Routine.");

            BinaryFormatter binaryFormatter = new BinaryFormatter();

            FileStream file;

            if (File.Exists(Application.persistentDataPath + "/SavedGame.dat"))
            {
                file = File.Open(Application.persistentDataPath + "/SavedGame.dat", FileMode.Open);
            }
            else
            {
                file = File.Create(Application.persistentDataPath + "/SavedGame.dat");
            }

            binaryFormatter.Serialize(file, data);
            file.Close();

        }


    }

}
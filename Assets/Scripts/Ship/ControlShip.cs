﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Gaminho;

public class ControlShip : MonoBehaviour
{
    #region Public

    public float Velocity  = 10f;
    public float SpeedRotation = 200.0f;
    public ControlGame controlGame;   
    public Life life;
    public GameObject MotorAnimation;
    public GameObject Shield;
    public GameObject Explosion;
    public Shot[] Shots;

    public Transform ShieldLife;
    public GameObject ShieldBarObject;

    public Transform ghostTransform;
    public Transform ghostGun;
    public bool hasGhost;
    public float ghostDelay = 1f;
    Queue<FrameData> frameDatas = new Queue<FrameData>();
    FrameData copiedFrameData;
    #endregion

    #region Private
    private Vector3 startPos;
    private bool shooting = false;
    private int lifeShield;
    private bool shotThisFrame = false;
    private float StartTime;
    #endregion

    void Start()
    {
        StartTime = Time.time;
        Statics.Player = gameObject.transform;
        if (!hasGhost)
            ghostTransform.gameObject.SetActive(false);

        if (GetComponent<Rigidbody2D>() == null)
        {
            Debug.LogError("Component required Rigidbody2D");
            Destroy(this);
            return;
        }

        if (GetComponent<BoxCollider2D>() == null)
        {
            Debug.LogWarning("BoxCollider2D not found, adding ...");
            gameObject.AddComponent<BoxCollider2D>();
            
        }

        startPos = transform.localPosition;
        GetComponent<Rigidbody2D>().gravityScale = 0.001f;

        StartCoroutine(Shoot());
    }

    void LateUpdate()
    {
        #region Move
        float rotation = Input.GetAxis("Horizontal") * SpeedRotation;
        rotation *= Time.deltaTime;
        transform.Rotate(0, 0, -rotation);


        if (Input.GetAxis("Vertical") != 0)
        {
            Vector2 translation = Input.GetAxis("Vertical") * new Vector2(0, Velocity * GetComponent<Rigidbody2D>().mass);
            translation *= Time.deltaTime;
            GetComponent<Rigidbody2D>().AddRelativeForce(translation, ForceMode2D.Impulse);
        }
        AnimateMotor();

        if (transform.localPosition.y > controlGame.ScenarioLimit.yMax || transform.localPosition.y < controlGame.ScenarioLimit.yMin || transform.localPosition.x > controlGame.ScenarioLimit.xMax || transform.localPosition.x < controlGame.ScenarioLimit.xMin)
        {
            Vector3 dir = startPos - transform.localPosition;
            dir = dir.normalized;
            GetComponent<Rigidbody2D>().AddForce(dir * (2 * GetComponent<Rigidbody2D>().mass), ForceMode2D.Impulse);

        }
        #endregion

        #region Tiro
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            shooting = true;
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            shooting = false;
        }

        #endregion

        //Frame Data is defined at the end of this script
        #region Ghost
        if (hasGhost) {
            //always save frame data if we have a ghost
            frameDatas.Enqueue(CopyFrameData(shotThisFrame));

            //only start moving the ghost if the delay already started
            if (Time.time >= StartTime + ghostDelay)
            {
                copiedFrameData = frameDatas.Dequeue();
                ghostTransform.position = copiedFrameData.pos;
                ghostTransform.rotation = copiedFrameData.rotation;

				if (copiedFrameData.shotSomething)
				{
                    //Here we're just shooting the current bullet on the main gun of the ghost.
                    //We could make the gun shoot 2 or 3 bullets like the player
                    //By recording what kind of shoot (Statics.TYPE_SHOT) at that frame
                    //And have a reference for the 3 gun positions.
                    //Then select what guns to fire depending on the TYPE_SHOT value.
                    //Since it's a ghost, having 6 shots might be overkill and too strong.
                    Statics.Damage = Shots[Statics.ShootingSelected].Damage;
                    GameObject goShoot = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goShoot.transform.parent = ghostTransform;
                    goShoot.transform.localPosition = ghostGun.localPosition;
                    goShoot.GetComponent<Rigidbody2D>().AddForce(ghostTransform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShoot.AddComponent<BoxCollider2D>();
                    goShoot.transform.parent = transform.parent;
                }
            }

            shotThisFrame = false;
        }
		#endregion

		#region Shield

		if (Shield.gameObject.activeInHierarchy)
		{
            ShieldBarObject.SetActive(true);
		} else ShieldBarObject.SetActive(false);

        ShieldLife.localScale = new Vector3(lifeShield / 10f, 1, 1);
        ShieldBarObject.transform.position = transform.position + Vector3.up * 180;
        #endregion

        Statics.Life = life.life;
    }
    
    private void AnimateMotor()
    {
        if(MotorAnimation.activeSelf != (Input.GetAxis("Vertical") != 0))
        {
            MotorAnimation.SetActive(Input.GetAxis("Vertical") != 0);
        }
    }

    private IEnumerator Shoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(Shots[Statics.ShootingSelected].ShootingPeriod);
            if (shooting)
            {
                shotThisFrame = true;
                Statics.Damage = Shots[Statics.ShootingSelected].Damage;
                GameObject goShoot = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                goShoot.transform.parent = transform;
                goShoot.transform.localPosition = Shots[Statics.ShootingSelected].Weapon.transform.localPosition;
                goShoot.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShoot.AddComponent<BoxCollider2D>();
                goShoot.transform.parent = transform.parent;

                if(Shots[Statics.ShootingSelected].TypeShooter == Statics.TYPE_SHOT.DOUBLE)
                {
                    GameObject goShoot2 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goShoot2.transform.parent = transform;
                    goShoot2.transform.localPosition = Shots[Statics.ShootingSelected].Weapon2.transform.localPosition;
                    goShoot2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShoot2.AddComponent<BoxCollider2D>();
                    goShoot2.transform.parent = transform.parent;
                }

                if (Shots[Statics.ShootingSelected].TypeShooter == Statics.TYPE_SHOT.TRIPLE)
                {
                    GameObject goShoot2 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goShoot2.transform.parent = transform;
                    goShoot2.transform.localPosition = Shots[Statics.ShootingSelected].Weapon2.transform.localPosition;
                    goShoot2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShoot2.AddComponent<BoxCollider2D>();
                    goShoot2.transform.parent = transform.parent;

                    GameObject goTiro3 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goTiro3.transform.parent = transform;
                    goTiro3.transform.localPosition = Shots[Statics.ShootingSelected].Weapon3.transform.localPosition;
                    goTiro3.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goTiro3.AddComponent<BoxCollider2D>();
                    goTiro3.transform.parent = transform.parent;
                }
            }

            CallShield();
        }
    }

    private void CallShield()
    {
        if(Shield.activeSelf != Statics.WithShield)
        {
            Shield.SetActive(Statics.WithShield);
        }
    }

    private void OnCollisionEnter2D(Collision2D obj)
    {
       
        if (obj.gameObject.tag == "Enemy")
        {
            Instantiate(Explosion, transform);
            
            if (Statics.WithShield)
            {
                lifeShield--;
            }
            else
            {
               
                life.TakesLife(obj.gameObject.GetComponent<Enemy>().Damage);
            }

        }


        //removed player`s hability to collide with own shots
        if ( obj.gameObject.tag == "EnemyShot")
        {
            Instantiate(Explosion, transform);
 
            if (Statics.WithShield)
            {
                lifeShield--;
            }
            else
            {
                
                life.TakesLife(1);
            }
            Destroy(obj.gameObject);
        }
        
        if(lifeShield <= 0)
        {
            ShieldBarObject.SetActive(false);
            Statics.WithShield = false;
            lifeShield = 10;
        }
        
        
    }

    //Struct of Data we need to record for P2 to copy
    public struct FrameData
    {
       public Vector2 pos;
       public Quaternion rotation;

        public bool shotSomething;
    }

    //helper function that copies P1 data into a FrameData struct;
    private FrameData CopyFrameData(bool shotSmth)
    {
        FrameData fData = new FrameData();
        fData.pos = transform.position;
        fData.rotation = transform.rotation;
        fData.shotSomething = shotSmth;
        return fData;
    }
}



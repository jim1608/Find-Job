﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CallScene : MonoBehaviour {


    public void Call(string sname)
    {
        //Aqui checamos se o level atual é um level de jogo
        //Assim mantemos o score inicial dele se for o ultimo level salvo.

        //Se vocë estiver recarregando esta cena.
        if (sname == "Game" && SceneManager.GetActiveScene().name == sname)
        {
            //e você passou de pontos
            if (Statics.Points > GameSave.data.latestScore)
			{
                Statics.Points = GameSave.data.latestScore;
			}
		}


        GameObject.Instantiate(Resources.Load(Statics.PREFAB_LOAD) as GameObject);
        SceneManager.LoadScene(sname);

    }
}
